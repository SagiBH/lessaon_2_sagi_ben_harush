#pragma once
#include "Mitochondrion.h"
#include "Ribosome.h"
#include "Nucleus.h"
#include "Protein.h"
#include "AminoAcid.h"

#define ATP 100

class Cell {
public:
	void init(const string dna_sequence, const Gene glucose_receptor_gene);

	bool get_ATP();
	Gene get_Gene();
	string get_DNA() const;
private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;
};