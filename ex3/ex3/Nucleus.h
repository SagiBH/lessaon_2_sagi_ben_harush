#pragma once
#include <iostream>

using namespace std;


class Gene {
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;

	void set_start(const unsigned int new_start);
	void set_end(const unsigned int new_end);


private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
};

class Nucleus {
public:
	void init(const string dna_sequence);

	string get_RNA_transcript(const Gene& gene) const;
	string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codons_appearances(const string& codon) const;
	string get_DNA_strand() const;

private:
	string _DNA_strand;
	string _complementary_DNA_strand;
};




