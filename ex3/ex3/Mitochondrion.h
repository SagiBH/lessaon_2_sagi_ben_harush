#pragma once
#include "Ribosome.h"

#define MIN_LEVEL_OF_GLOCUSE 50

class Mitochondrion {
public:
	void init();

	void insert_glocuse_receptor(const Protein& protein);
	void set_glocuse(const unsigned int glocuse_units);
	bool produceATP() const;

private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;
};