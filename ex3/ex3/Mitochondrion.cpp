#include "Mitochondrion.h"

//Mitochondrion init
void Mitochondrion::init() {
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

/*
the function inserts the glocuse receptor
input: protein
output: none
*/
void Mitochondrion::insert_glocuse_receptor(const Protein& protein) {
	AminoAcidNode* head = protein.get_first();

	if (head != nullptr && head->get_data() == ALANINE) {
		head = head->get_next();
		if (head != nullptr && head->get_data() == LEUCINE) {
			head = head->get_next();
			if (head != nullptr && head->get_data() == GLYCINE) {
				head = head->get_next();
				if (head != nullptr && head->get_data() == HISTIDINE) {
					head = head->get_next();
					if (head != nullptr && head->get_data() == LEUCINE) {
						head = head->get_next();
						if (head != nullptr && head->get_data() == PHENYLALANINE) {
							head = head->get_next();
							if (head != nullptr && head->get_data() == AMINO_CHAIN_END) {
								this->_has_glocuse_receptor = true;
							}
						}
					}
				}

			}
		}
	}
}

//Mitochondrion setter
void Mitochondrion::set_glocuse(const unsigned int glocuse_units) {
	this->_glocuse_level = glocuse_units;
}

/*
the function checks if it can produce atp
input: none
output: if yes true if not false
*/
bool Mitochondrion::produceATP() const {
	if (this->_has_glocuse_receptor && this->_glocuse_level >= MIN_LEVEL_OF_GLOCUSE) {
		return true;
	}
	return false;
}