#include "Cell.h"

// cell init
void Cell::init(const string dna_sequence, const Gene glucose_receptor_gene) {
	
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
	this->_atp_units = 0;
	this->_glocus_receptor_gene.init(glucose_receptor_gene.get_start(), glucose_receptor_gene.get_end(), glucose_receptor_gene.is_on_complementary_dna_strand());
}

/*
this function charges the ATP in the cell
input: none
output: true when you get ATP
*/
bool Cell::get_ATP() {
	
	Protein* protien = new Protein;
	string rna = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	protien = this->_ribosome.create_protein(rna);

	if (protien == nullptr) {
		cerr << "Error! unknown protien" << endl;
		_exit(1);
	}

	this->_mitochondrion.set_glocuse(MIN_LEVEL_OF_GLOCUSE);
	this->_mitochondrion.insert_glocuse_receptor(*protien);

	if (!(this->_mitochondrion.produceATP())) {
		return false;
	}

	this->_atp_units = ATP;
	return true;
}

//getters
Gene Cell::get_Gene() {
	return this->_glocus_receptor_gene;
}

string Cell::get_DNA() const {
	return (this->_nucleus).get_reversed_DNA_strand();
}