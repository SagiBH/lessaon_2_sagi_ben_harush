#include <iostream>
#include <string>
#include <regex>
#include "Nucleus.h"


//Gene init
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand) {
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

//Gene getters
unsigned int Gene::get_start() const {
	return this->_start;
}

unsigned int Gene::get_end() const {
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const {
	return this->_on_complementary_dna_strand;
}

//Gene setters
void Gene::set_start(const unsigned int new_start) {
	this->_start = new_start;
}

void Gene::set_end(const unsigned int new_end) {
	this->_end = new_end;
}


/*
this function creates the complementary DNA strand and initialize the nucleus 
input: dna_sequence
output: none
*/
void Nucleus::init(const string dna_sequence)
{
	int i = 0;
	int size = 0;
	string complementar;

	_complementary_DNA_strand = "";
	this->_DNA_strand = dna_sequence;
	size = dna_sequence.length();
	string complementary = dna_sequence;

	for (i = 0; i < size; i++)
	{
		switch (complementary[i])
		{
		case('A'):
			this->_complementary_DNA_strand = this->_complementary_DNA_strand + 'T';
			break;
		case('C'):
			this->_complementary_DNA_strand = this->_complementary_DNA_strand + 'G';
			break;
		case('T'):
			this->_complementary_DNA_strand = this->_complementary_DNA_strand + 'A';
			break;
		case('G'):
			this->_complementary_DNA_strand = this->_complementary_DNA_strand + 'C';
			break;
		default:
			cerr << "error! unknown nucleotide...";
			_exit(1);
		}
	}
}

/*
this function creates the RNA transcript
input: gene
output: RNA transcript
*/
string Nucleus::get_RNA_transcript(const Gene& gene) const {
	string RNA_transcript = "";
	if (gene.is_on_complementary_dna_strand() == false) {
		for (size_t i = gene.get_start(); i <= gene.get_end(); i++)
		{
			if (this->_DNA_strand[i] == 'T') {
				RNA_transcript += "U";
			}
			else {
				RNA_transcript += this->_DNA_strand[i];
			}
		}
	}
	if (gene.is_on_complementary_dna_strand() == true) {
		for (size_t i = gene.get_start(); i <= gene.get_end(); i++)
		{
			if (this->_complementary_DNA_strand[i] == 'T') {
				RNA_transcript += "U";
			}
			else {
				RNA_transcript += this->_complementary_DNA_strand[i];
			}
		}
	}
	return RNA_transcript;
}

//Nucleus getters
string Nucleus::get_reversed_DNA_strand() const {
	string reversed_DNA_strand = string(this->_DNA_strand.end(), this->_DNA_strand.begin());
	return reversed_DNA_strand;
}

unsigned int Nucleus::get_num_of_codons_appearances(const string& codon) const {
	unsigned int num_of_codons_appearances = 0;
	regex re(codon);
	num_of_codons_appearances = distance(sregex_iterator(this->_DNA_strand.begin(), this->_DNA_strand.end(), re), sregex_iterator());

	return num_of_codons_appearances;
}

string Nucleus::get_DNA_strand() const {
	return this->_DNA_strand;
}
