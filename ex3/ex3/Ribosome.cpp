#include "Ribosome.h"

/*
the function create the protein from the rna
input: rna
output: protein
*/
Protein* Ribosome::create_protein(string& RNA_transcript) const
{
    Protein* p = new Protein;
    AminoAcid acid;
    string s;
    int beg = 0;
    int end = 3;
    int size = RNA_transcript.length();

    string rna = RNA_transcript;
    p->init();

    while (size - beg >= 3)
    {

        s = RNA_transcript.substr(beg, end);
        acid = get_amino_acid(s);

        if (acid == UNKNOWN)
        {
            p->clear();
            return nullptr;
        }

        p->add(acid);
        beg += 3;
    }

    return p;
}