#pragma once
#include "Protein.h"

using namespace std;

class Ribosome {
public:
	Protein* create_protein(string& RNA_transcript) const;
};